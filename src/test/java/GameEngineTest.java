import com.mscharhag.oleaster.runner.OleasterRunner;
import config.GameConfig;
import game.Choice;
import game.GameEngine;
import history.GameStorage;
import org.junit.runner.RunWith;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.HashMap;

import static com.mscharhag.oleaster.matcher.Matchers.expect;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;

@RunWith(OleasterRunner.class)
public class GameEngineTest {{

    describe("GameEngine", () -> {

        ByteArrayOutputStream outContent = new ByteArrayOutputStream();
        ByteArrayOutputStream errContent = new ByteArrayOutputStream();
        PrintStream originalOut = System.out;
        PrintStream originalErr = System.err;

        GameEngine engine = new GameEngine(new GameStorage(new HashMap<>(), new ArrayList<>()));

        ByteArrayInputStream in = new ByteArrayInputStream("Andrew\n2\n2".getBytes());
        System.setIn(in);

        before(() ->{
            System.setOut(new PrintStream(outContent));
            System.setErr(new PrintStream(errContent));
        });

        after(() ->{
            System.setOut(originalOut);
            System.setErr(originalErr);
            System.setIn(System.in);
        });

        it("should report tie when both users choose same figure", () -> {
            Choice userChoice = new Choice("User", GameConfig.gameCommands.get("0"));
            Choice computerChoice = new Choice("Computer", GameConfig.gameCommands.get("0"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual("NONE");
        });

        it("should report User1 won if User1 use PAPER and User2 use ROCK", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("1"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("0"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use PAPER and User2 use SPOCK", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("1"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("4"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use SCISSORS and User2 use PAPER", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("2"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("1"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use SCISSORS and User2 use LIZARD", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("2"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("3"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use ROCK and User2 use LIZARD", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("0"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("3"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use ROCK and User2 use SCISSORS", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("0"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("2"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use LIZARD and User2 use SPOCK", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("3"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("4"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use LIZARD and User2 use PAPER", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("3"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("1"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use SPOCK and User2 use SCISSORS", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("4"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("2"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });

        it("should report User1 won if User1 use SPOCK and User2 use ROCK", () -> {
            String user1 = "User1";
            String user2 = "User2";
            Choice userChoice = new Choice(user1, GameConfig.gameCommands.get("4"));
            Choice computerChoice = new Choice(user2, GameConfig.gameCommands.get("0"));
            expect(engine.judge(userChoice, computerChoice).winner()).toEqual(user1);
        });


        it("should be able to start engine, after engine started user and computer layers have to be created", () -> {
            engine.start();
            expect(engine.user().name()).toEqual("Andrew");
            expect(engine.computer().name()).toEqual("COMPUTER");
        });

        it("should be able to see statistics after game started", () -> {
            engine.executeMenuCommand("SHOW STATISTICS");
            expect(outContent.toString()).toEqual(
                    "Welcome to Rock Scissors Paper Game!\n" +
                            "[v.0.1 Lizard Spock Extension]\n" +
                            "\n" +
                            "What is your player name? Name: STATISTICS FOR Andrew\n" +
                            "TOTAL GAMES = 0 | WINNER TIMES = 0 |  LOOSER TIMES = 0 | TIE TIMES = 0 | SUCCESS RATE = 0.0 %\n");
        });

        it("should be able to start game session and see the results of a game, update profile and history", () -> {
            engine.executeMenuCommand("PLAY NEW ROUND");
            expect(outContent.toString()).toContain("=====   The winner is ->");
            expect(engine.statisticsStorage().totalGamesByUser("Andrew")).toEqual(1);
            expect(engine.statisticsStorage().history().size()).toEqual(1);
        });


    });
}}
