import com.mscharhag.oleaster.runner.OleasterRunner;
import helpers.InputValidatior;
import org.junit.runner.RunWith;

import static com.mscharhag.oleaster.matcher.Matchers.expect;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.describe;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.it;

@RunWith(OleasterRunner.class)
public class InputValidatorTest {{

    describe("InputValidator", () -> {

        it("should return true if UserName is alpanumeric", () -> {
            expect(InputValidatior.isValidUserName("Andy67")).toBeTrue();
        });

        it("should return false if UserName is Not alpanumeric", () -> {
            expect(InputValidatior.isValidUserName("###")).toBeFalse();
        });

        it("should return false if UserName is empty", () -> {
            expect(InputValidatior.isValidUserName("")).toBeFalse();
        });

        it("should return false if UserName is longer than 20 char", () -> {
            expect(InputValidatior.isValidUserName("sadasdasdasdasdasdasdasdasdasdasdasdasdas")).toBeFalse();
        });

        it("should return true if user provided menu choice from 0 to 2", () -> {
            expect(InputValidatior.isCommandValid("1")).toBeTrue();
        });


        it("should return false if user provided menu choice NOT from 0 to 2", () -> {
            expect(InputValidatior.isCommandValid("3")).toBeFalse();
        });

        it("should return false if user provided menu choice NOT number", () -> {
            expect(InputValidatior.isCommandValid("a")).toBeFalse();
        });

        it("should return true if user provided game choice from 0 to 4", () -> {
            expect(InputValidatior.isGameChoiceValid("1")).toBeTrue();
            expect(InputValidatior.isGameChoiceValid("2")).toBeTrue();
            expect(InputValidatior.isGameChoiceValid("3")).toBeTrue();
            expect(InputValidatior.isGameChoiceValid("4")).toBeTrue();
            expect(InputValidatior.isGameChoiceValid("0")).toBeTrue();
        });

        it("should return true if user provided game choice NOT from 0 to 4", () -> {
            expect(InputValidatior.isGameChoiceValid("6")).toBeFalse();
            expect(InputValidatior.isGameChoiceValid("7")).toBeFalse();
            expect(InputValidatior.isGameChoiceValid("-1")).toBeFalse();
            expect(InputValidatior.isGameChoiceValid("r")).toBeFalse();
            expect(InputValidatior.isGameChoiceValid("")).toBeFalse();
        });


    });
}}
