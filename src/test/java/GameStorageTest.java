import com.mscharhag.oleaster.runner.OleasterRunner;
import config.GameConfig;
import game.Choice;
import game.GameEngine;
import helpers.SerializationHelper;
import history.GameRound;
import history.GameStorage;
import history.PlayerProfile;
import org.junit.Rule;
import org.junit.contrib.java.lang.system.SystemOutRule;
import org.junit.contrib.java.lang.system.TextFromStandardInputStream;
import org.junit.runner.RunWith;
import player.Player;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import java.util.*;

import static com.mscharhag.oleaster.matcher.Matchers.expect;
import static com.mscharhag.oleaster.runner.StaticRunnerSupport.*;
import static org.junit.contrib.java.lang.system.TextFromStandardInputStream.emptyStandardInputStream;

@RunWith(OleasterRunner.class)
public class GameStorageTest {{

    describe("GameStorage", () -> {

        String storageFileName = "testStorage" + new Date().toString()+".ser";

        PlayerProfile andrew = new PlayerProfile("Andrew");
        andrew.win();
        andrew.win();
        andrew.win();

        PlayerProfile john = new PlayerProfile("John");
        john.win();
        john.win();
        john.lost();
        john.lost();

        PlayerProfile kate = new PlayerProfile("Kate");
        kate.tie();
        kate.tie();
        kate.win();
        kate.win();

        PlayerProfile gogo = new PlayerProfile("Gogo");
        gogo.lost();
        gogo.lost();
        gogo.lost();
        gogo.win();

         Map<String, PlayerProfile> profiles = new HashMap<String, PlayerProfile>() {
            {
                put("Andrew", andrew);
                put("John", john);
                put("Kate", kate);
                put("Gogo", gogo);
            }
        };

        GameStorage storage = new GameStorage(profiles, new ArrayList<>());

        after(() ->{
            File file = new File(storageFileName);

            if(file.delete())
            {
                System.out.println("File deleted successfully");
            }
            else
            {
                System.out.println("Failed to delete the file");
            }
        });

        it("should return true profile if success rate is higher than 50%", () -> {
            expect(storage.isWinnerProfile("Andrew")).toBeTrue();
        });

        it("should return false profile if success rate is 50%", () -> {
            expect(storage.isWinnerProfile("John")).toBeFalse();
        });

        it("should return true profile if success rate is 75%", () -> {
            expect(storage.isWinnerProfile("Kate")).toBeTrue();
        });

        it("should return false profile if success rate is less than 50%", () -> {
            expect(storage.isWinnerProfile("Gogo")).toBeFalse();
        });

        it("should return correct total games played by user", () -> {
            expect(storage.totalGamesByUser("Gogo")).toEqual(4);
        });

        it("should return new profile if profile doesn't exist", () -> {
            expect(storage.getProfileByName("Unknown")).toBeNotNull();
            expect(storage.totalGamesByUser("Unknown")).toEqual(0);
        });

        it("should return correct user success rate", () -> {
            expect(storage.successRate(storage.getProfileByName("Unknown"))).toEqual(0);
            expect(storage.successRate(storage.getProfileByName("Kate"))).toEqual(75);
            expect(storage.successRate(storage.getProfileByName("Andrew"))).toEqual(100);
            expect(storage.successRate(storage.getProfileByName("John"))).toEqual(50);
        });


        it("should update statistics and history after avery game", () -> {
            storage.updateStatistics(new GameRound(
                    "Andrew",
                    GameConfig.gameCommands.get("1").tieWinNames().get(0),
                    "Kate",
                    GameConfig.gameCommands.get("2").tieWinNames().get(0),
                    "Kate"));
            expect(storage.getProfileByName("Andrew").lostTimes()).toEqual(1);
            expect(storage.getProfileByName("Kate").winnerTimes()).toEqual(3);
            expect(storage.history().size()).toEqual(1);
        });

        it("should be able to get default storage from file if it not exists", () -> {
            GameStorage receivedStorage = SerializationHelper.getStorage(storageFileName);
            PlayerProfile profile = receivedStorage.getProfileByName("TestProfile");
            PlayerProfile computer = receivedStorage.getProfileByName("Computer");
            profile.win();
            receivedStorage.updateStatistics(new GameRound(
                    profile.name(),
                    GameConfig.gameCommands.get("1").tieWinNames().get(0),
                    computer.name(),
                    GameConfig.gameCommands.get("2").tieWinNames().get(0),
                    computer.name()));
            SerializationHelper.saveStorageToFile(storageFileName, receivedStorage);
            expect(receivedStorage.getProfileByName(profile.name()).winnerTimes()).toEqual(1);
            expect(receivedStorage.getProfileByName(computer.name()).winnerTimes()).toEqual(1);
            expect(receivedStorage.history().size()).toEqual(1);
        });


        it("should be able to restore storage from file", () -> {
            GameStorage receivedStorage = SerializationHelper.getStorage(storageFileName);
            SerializationHelper.saveStorageToFile(storageFileName, receivedStorage);
            expect(receivedStorage.getProfileByName("TestProfile").winnerTimes()).toEqual(1);
            expect(receivedStorage.getProfileByName("Computer").winnerTimes()).toEqual(1);
            expect(receivedStorage.history().size()).toEqual(1);
        });

    });
}}
