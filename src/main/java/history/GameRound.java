package history;



import java.io.Serializable;

public class GameRound implements Serializable {

    private final String firstPlayerName;
    private final String firstPlayerChoice;
    private final String secondPlayerName;
    private final String secondPlayerChoice;
    private final String winner;

    public String firstPlayerName() {
        return firstPlayerName;
    }

    public String firstPlayerChoice() {
        return firstPlayerChoice;
    }

    public String secondPlayerName() {
        return secondPlayerName;
    }

    public String secondPlayerChoice() {
        return secondPlayerChoice;
    }

    public String winner() {
        return winner;
    }


    public GameRound(String firstPlayerName, String firstPlayerChoice, String secondPlayerName, String secondPlayerChoice, String winner) {
        this.firstPlayerName = firstPlayerName;
        this.firstPlayerChoice = firstPlayerChoice;
        this.secondPlayerName = secondPlayerName;
        this.secondPlayerChoice = secondPlayerChoice;
        this.winner = winner;
    }
}
