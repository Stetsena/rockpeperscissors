package history;


import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * The type Game storage.
 */
public class GameStorage implements Serializable {

    /**
     * The Profiles.
     */
    Map<String, PlayerProfile> profiles;

    /**
     * The History.
     */
    List<GameRound> history;

    /**
     * Instantiates a new Game storage.
     *
     * @param profiles the profiles
     * @param history  the history
     */
    public GameStorage(Map<String, PlayerProfile> profiles, List<GameRound> history) {
        this.profiles = profiles;
        this.history = history;
    }

    /**
     * History list.
     *
     * @return the list
     */
    public List<GameRound> history() {
        return history;
    }

    /**
     * Gets profile by name.
     *
     * @param name the name
     * @return the profile by name
     */
    public PlayerProfile getProfileByName(String name) {
        if(!profiles.containsKey(name)){
            profiles.put(name, new PlayerProfile(name));
        }
        return profiles.get(name);
    }

    /**
     * Update statistics.
     *
     * @param round the round
     */
    public void updateStatistics(GameRound round) {
        String winner = round.winner();
        if("NONE".equals(winner)) {
            profiles.get(round.firstPlayerName()).tie();
            profiles.get(round.secondPlayerName()).tie();
        } else {
            profiles.get(winner).win();
            if (round.firstPlayerName().equals(winner)) {
                profiles.get(round.secondPlayerName()).lost();
            } else {
                profiles.get(round.firstPlayerName()).lost();
            }
        }
        history.add(round);
    }

    /**
     * Is winner profile boolean.
     *
     * @param name the name
     * @return the boolean
     */
    public boolean isWinnerProfile(String name) {
        return successRate(getProfileByName(name)) > 50;
    }

    /**
     * Total games by user.
     *
     * @param name the name
     * @return the long
     */
    public long totalGamesByUser(String name){
        PlayerProfile profile = getProfileByName(name);
        return profile.winnerTimes() + profile.tieTimes() + profile.lostTimes();
    }

    /**
     * Success rate calculation.
     *
     * @param userProfile the user profile
     * @return the double
     */
    public double successRate(PlayerProfile userProfile) {
        long totalGames = userProfile.winnerTimes() + userProfile.tieTimes() + userProfile.lostTimes();
        if(totalGames>0) {
            double halfTies = userProfile.tieTimes() / 2;
            return ((userProfile.winnerTimes() + halfTies) / totalGames) * 100;
        } else {
            return 0;
        }
    }



}
