package history;

import java.io.Serializable;

public class PlayerProfile implements Serializable {

    private final String name;

    long winnerTimes = 0;
    long lostTimes = 0;
    long tieTimes = 0;

    public String name() {
       return name;
    }

    public void win() {
        winnerTimes++;
    }

    public void lost() {
        lostTimes++;
    }

    public void tie() {
        tieTimes++;
    }

    public long winnerTimes() {
        return winnerTimes;
    }

    public long lostTimes() {
        return lostTimes;
    }

    public long tieTimes() {
        return tieTimes;
    }

    public PlayerProfile(String name) {
        this.name = name;
    }


}
