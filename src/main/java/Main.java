import config.GameConfig;
import game.GameEngine;
import helpers.SerializationHelper;
import history.GameStorage;

import java.io.IOException;


public class Main {

    public static void main(String[] args) {
        try {
            GameStorage storage = SerializationHelper.getStorage(GameConfig.STORAGE_FILENAME);
            GameEngine engine = new GameEngine(storage);
            engine.start();
            engine.processCommands();
        } catch (IOException e) {
            System.out.println("ERROR: Can't deserialize GameStorage " + e.getMessage());
        } catch (ClassNotFoundException e) {
            System.out.println("ERROR: GameStorage class not exists");
        }
    }
}
