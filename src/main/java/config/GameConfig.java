package config;

import game.Selectable;

import java.util.HashMap;
import java.util.Map;

import static helpers.CollectionsHelper.immutableList;

public class GameConfig {

    public static final String STORAGE_FILENAME = "storage.ser";
    public static final String SHOW_STATISTICS = "SHOW STATISTICS";
    public static final String NEW_ROUND = "PLAY NEW ROUND";
    public static final String QUIT = "QUIT GAME";
    public static final String ROCK = "ROCK";
    public static final String SCISSORS = "SCISSORS";
    public static final String PAPER = "PAPER";
    public static final String LIZARD = "LIZARD";
    public static final String SPOCK = "SPOCK";

    public static final String userNameValidationPattern ="[A-Za-z1-9]{1,20}";

    //I Left key as String because commands might now only be numbers
    public static final Map<String, String> menuCommands = new HashMap<String, String>() {
        {
            put("2", SHOW_STATISTICS);
            put("1", NEW_ROUND);
            put("0", QUIT);
        }
    };

    public static final String menuValidationPattern = String.format("[0-%s]{1}",String.valueOf(menuCommands.size()-1));

    //I Left key as String because commands might now only be numbers
    public static final Map<String, Selectable> gameCommands = new HashMap<String, Selectable>() {
        {
            put("0", () -> immutableList(ROCK, SCISSORS, LIZARD));
            put("1", () -> immutableList(PAPER, ROCK, SPOCK));
            put("2", () -> immutableList(SCISSORS, PAPER, LIZARD));
            put("3", () -> immutableList(LIZARD, PAPER, SPOCK));
            put("4", () -> immutableList(SPOCK, ROCK, SCISSORS));
        }
    };

    public static final String gameChoiceValidationPattern = String.format("[0-%s]{1}",String.valueOf(gameCommands.size()-1));



}
