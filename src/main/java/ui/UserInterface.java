package ui;

import game.Selectable;
import helpers.InputValidatior;

import java.util.Scanner;

import static config.GameConfig.gameCommands;
import static config.GameConfig.menuCommands;

/**
 * The type User interface.
 */
public class UserInterface {

    /**
     * The Scanner.
     */
    static Scanner scanner = new Scanner(System.in);

    /**
     * Show app start wellcome.
     */
    public static void showAppStartWellcome() {
        System.out.println("Welcome to Rock Scissors Paper Game!");
        System.out.println("[v.0.1 Lizard Spock Extension]");
        System.out.println("");
    }

    /**
     * Ask user name string.
     *
     * @return the string
     */
    public static String askUserName() {
        System.out.print("What is your player name? ");
        System.out.print("Name: ");
        String username = scanner.next();
        if(InputValidatior.isValidUserName(username)){
          return username;
        } else {
            System.out.print("Invalid UserName Format, Please use only Characters and Numbers");
            return askUserName();
        }
    }

    /**
     * Show game start welcome.
     *
     * @param playerName            the player name
     * @param computerName          the computer name
     * @param isStatisticallyWinner the is statistically winner
     */
    public static void showGameStartWelcome(String playerName, String computerName,  Boolean isStatisticallyWinner) {
        System.out.println(String.format("\nWelcome to new game round, %s!", playerName));
        String curseText = isStatisticallyWinner? "Lucky ass" : "Looser";
        System.out.println(String.format("[%s]: Hello %s, make your move!", computerName, curseText));
        System.out.println("");
    }

    /**
     * Show round results.
     *
     * @param firstPlayerName    the first player name
     * @param firstPlayerChoice  the first player choice
     * @param secondPlayerName   the second player name
     * @param secondPlayerChoice the second player choice
     * @param winner             the winner
     */
    public static void showRoundResults(String firstPlayerName, String firstPlayerChoice, String secondPlayerName, String secondPlayerChoice, String winner) {
        System.out.println(String.format("[%s] play %s", firstPlayerName, firstPlayerChoice));
        System.out.println(String.format("[%s] play %s", secondPlayerName, secondPlayerChoice));
        System.out.println(String.format("\n =====   The winner is -> %s! ====", winner));
        System.out.println("");
    }

    /**
     * Ask user menu command string.
     *
     * @return the string
     */
    public static String askUserMenuCommand() {
        System.out.print("\nWhat would you like to do next? -> ");
        menuCommands.entrySet().stream().forEach((entry) -> System.out.print(entry.getValue() + " ("+entry.getKey()+") | "));
        System.out.println();
        String command = scanner.next();
        if(InputValidatior.isCommandValid(command)){
            return menuCommands.get(command);
        } else {
            System.out.println("Invalid Move Format, Please use only Numbers from 0 to " + (menuCommands.size()-1));
            return askUserMenuCommand();
        }
    }

    /**
     * Display statistic.
     *
     * @param userName    the user name
     * @param wins        the wins
     * @param looses      the looses
     * @param ties        the ties
     * @param successRate the success rate
     */
    public static void displayStatistic(String userName, long wins, long looses, long ties, double successRate){
        System.out.println("STATISTICS FOR "+ userName);
        System.out.print(String.format("TOTAL GAMES = %s | WINNER TIMES = %s |  LOOSER TIMES = %s | TIE TIMES = %s | SUCCESS RATE = %s "
                ,wins+looses+ties, wins, looses, ties, successRate));
        System.out.println("%");
    }


    /**
     * Ask user game command selectable.
     *
     * @return the selectable
     */
    public static Selectable askUserGameCommand() {
        System.out.print("\nMake your Move ->  ");
        gameCommands.entrySet().stream().forEach((entry) -> System.out.print(entry.getValue().tieWinNames().get(0) + " ("+entry.getKey()+") | "));
        System.out.println();
        String command = scanner.next();
        if(InputValidatior.isGameChoiceValid(command)){
            return gameCommands.get(command);
        } else {
            System.out.println("Invalid Move Format, Please use only Numbers from 0 to " + gameCommands.size());
            return askUserGameCommand();
        }
    }


}
