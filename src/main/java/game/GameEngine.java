package game;

import config.GameConfig;
import helpers.SerializationHelper;
import history.GameRound;
import history.GameStorage;
import history.PlayerProfile;
import player.Computer;
import player.Human;
import player.Player;
import ui.UserInterface;

import java.util.HashMap;
import java.util.Map;

import static config.GameConfig.NEW_ROUND;
import static config.GameConfig.QUIT;
import static config.GameConfig.SHOW_STATISTICS;

/**
 * The type Game engine.
 */
public class GameEngine {

    private GameStorage statisticsStorage;

    private Map<String, Executable> commandHandler;

    private Player human;

    private Player computer;

    /**
     * Instantiates a new Game engine.
     *
     * @param statisticsStorage the statistics storage
     */
    public GameEngine(GameStorage statisticsStorage) {
        this.statisticsStorage = statisticsStorage;
        this.commandHandler = new HashMap<>();
        commandHandler.put(SHOW_STATISTICS, this::showStatistics);
        commandHandler.put(NEW_ROUND, this::playNewRound);
        commandHandler.put(QUIT, this::stop);
    }

    /**
     * User player.
     *
     * @return the player
     */
    public Player user() {
        return human;
    }

    /**
     * Computer player.
     *
     * @return the player
     */
    public Player computer() {
        return computer;
    }

    /**
     * Statistics storage game storage.
     *
     * @return the game storage
     */
    public GameStorage statisticsStorage() {
        return statisticsStorage;
    }


    /**
     * Start Game Engine.
     */
    public void start() {
        UserInterface.showAppStartWellcome();
        human = new Human(UserInterface.askUserName());
        computer = new Computer();
        statisticsStorage.getProfileByName(computer.name());
        statisticsStorage.getProfileByName(human.name());
    }

    /**
     * Recursive Processing of menu commands.
     */
    public void processCommands() {
        while(true) {
            String command = UserInterface.askUserMenuCommand();
            commandHandler.get(command).apply();
        }
    }


    /**
     * Execute menu command.
     *
     * @param command the command
     */
    public void executeMenuCommand(String command) {
        commandHandler.get(command).apply();
    }

    /**
     * Decide the winner.
     *
     * @param one the user move
     * @param two the user move
     * @return the game round
     */
    public GameRound judge(Choice one, Choice two) {
       return new GameRound(
               one.playerName(),
               one.choiceName(),
               two.playerName(),
               two.choiceName(),
               decideWinnerName(one, two));
    }

    private void showStatistics(){
        PlayerProfile profile = statisticsStorage.getProfileByName(human.name());
        UserInterface.displayStatistic(
                human.name(),
                profile.winnerTimes(),
                profile.lostTimes(),
                profile.tieTimes(),
                statisticsStorage.successRate(profile));
    }

    private void playNewRound(){

        UserInterface.showGameStartWelcome(
                human.name(),
                computer.name(),
                statisticsStorage.isWinnerProfile(human.name()));

        GameRound round = judge(human.generateChoice(), computer.generateChoice());

        statisticsStorage.updateStatistics(round);
        SerializationHelper.saveStorageToFile(GameConfig.STORAGE_FILENAME, statisticsStorage);

        UserInterface.showRoundResults(
                round.firstPlayerName(),
                round.firstPlayerChoice(),
                round.secondPlayerName(),
                round.secondPlayerChoice(),
                round.winner());
    }

    private String decideWinnerName(Choice one, Choice two) {
        switch(one.playerChoice().compareTo(two.playerChoice())) {
            case 0:
                return "NONE";
            case 1:
                return one.playerName();
            default:
                return two.playerName();
        }
    }

    private void stop() {
        SerializationHelper.saveStorageToFile(GameConfig.STORAGE_FILENAME, statisticsStorage);
        System.exit(0);
    }


}
