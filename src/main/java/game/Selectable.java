package game;

import java.util.List;

/**
 * The interface Selectable.
 */
public interface Selectable extends Comparable<Selectable> {

    /**
     * Get List with Elements that current figure can beat.
     * Note: First element is always itself, and it produces the tie situation.
     *
     * @return the list
     */
    List<String> tieWinNames();

    @Override
    default int compareTo(Selectable selectable) {
        String currentChoice = tieWinNames().get(0);
        String oppositeChoice = selectable.tieWinNames().get(0);
        if(currentChoice.equals(oppositeChoice)){
            return 0;
        } else if(tieWinNames().contains(oppositeChoice)){
            return 1;
        } else {
            return -1;
        }
    }
}
