package game;

import java.io.Serializable;

public class Choice implements Serializable{

    private final String playerName;
    private final Selectable playerChoice;

    public String playerName() {
        return playerName;
    }

    public Selectable playerChoice() {
        return playerChoice;
    }

    public String choiceName() {
        return playerChoice.tieWinNames().get(0);
    }

    public Choice(String playerName, Selectable playerChoice) {
        this.playerName = playerName;
        this.playerChoice = playerChoice;
    }
}
