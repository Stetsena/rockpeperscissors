package game;

public interface Executable {
    void apply();
}
