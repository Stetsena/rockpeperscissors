package player;

import config.GameConfig;
import game.Choice;

import java.util.Random;

public class Computer implements Player {

    @Override
    public String name() {
        return "COMPUTER";
    }

    @Override
    public Choice generateChoice() {
        return new Choice(name(), GameConfig.gameCommands.get(getRandomNumberInRange(0,4)));
    }

    private String getRandomNumberInRange(int min, int max) {

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        Random r = new Random();
        return String.valueOf(r.nextInt((max - min) + 1) + min);
    }
}
