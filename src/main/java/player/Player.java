package player;

import game.Choice;
import game.Selectable;

/**
 * The interface Player.
 */
public interface Player {

    /**
     * Get Name of Player.
     *
     * @return the string
     */
    String name();

    /**
     * Generate choice choice.
     *
     * @return the choice
     */
    Choice generateChoice();

}
