package player;

import game.Choice;
import game.Selectable;
import ui.UserInterface;

public class Human implements Player {

    private final String name;

    public Human(String name) {
        this.name = name;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public Choice generateChoice() {
        return new Choice(name, UserInterface.askUserGameCommand());
    }
}
