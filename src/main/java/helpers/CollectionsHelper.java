package helpers;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * The type Collections helper.
 */
public class CollectionsHelper {

    /**
     * Immutable list from elements.
     *
     * @param <T>      the type parameter
     * @param elements the elements
     * @return the list
     */
    public static <T> List<T> immutableList(T... elements) {
        return Collections.unmodifiableList(Arrays.asList(elements));
    }



}
