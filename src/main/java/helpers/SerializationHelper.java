package helpers;

import config.GameConfig;
import history.GameStorage;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * The type Serialization helper.
 */
public class SerializationHelper {

    /**
     * Save storage to file.
     *
     * @param fileName the file name
     * @param storage  the storage
     */
    public static void saveStorageToFile(String fileName, GameStorage storage) {
        try {
            FileOutputStream serialisationStream = new FileOutputStream(new File(fileName));
            ObjectOutputStream outputStream = new ObjectOutputStream(serialisationStream);
            outputStream.writeObject(storage);
        } catch (IOException e) {
           System.out.println("ERROR: Serialize storage failed");
        }
    }

    /**
     * Gets storage from file.
     *
     * @param fileName the file name
     * @return the storage
     * @throws IOException            the io exception
     * @throws ClassNotFoundException the class not found exception
     */
    public static GameStorage getStorage(String fileName) throws IOException, ClassNotFoundException {
        try {
            FileInputStream fi = new FileInputStream(new File(fileName));
            ObjectInputStream oi = new ObjectInputStream(fi);
            return (GameStorage) oi.readObject();
        } catch (FileNotFoundException e) {
            return new GameStorage(new HashMap<>(), new ArrayList<>());
        }
    }

}
