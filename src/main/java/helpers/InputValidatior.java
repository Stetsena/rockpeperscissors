package helpers;

import config.GameConfig;

import java.util.regex.Pattern;

import static config.GameConfig.gameChoiceValidationPattern;
import static config.GameConfig.menuValidationPattern;
import static config.GameConfig.userNameValidationPattern;

/**
 * The type Input validatior.
 */
public class InputValidatior {

    /**
     * Is command valid boolean.
     *
     * @param input the input
     * @return the boolean
     */
    public static boolean isCommandValid(String input) {
       return isValid(input, menuValidationPattern);
    }

    /**
     * Is game choice valid boolean.
     *
     * @param input the input
     * @return the boolean
     */
    public static boolean isGameChoiceValid(String input) {
        return isValid(input,gameChoiceValidationPattern);
    }

    /**
     * Is valid user name boolean.
     *
     * @param input the input
     * @return the boolean
     */
    public static boolean isValidUserName(String input) {
        return isValid(input,userNameValidationPattern);
    }

    /**
     * Is valid boolean.
     *
     * @param input         the input
     * @param patternString the pattern string
     * @return the boolean
     */
    static boolean isValid(String input, String patternString) {
        final Pattern pattern = Pattern.compile(patternString);
        if (!pattern.matcher(input).matches()) {
            return false;
        }
        return true;
    }

}
